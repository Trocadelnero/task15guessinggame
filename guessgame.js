// Task 15 - Guessing Game
// Create a Guessing Game in JavaScript
// Guess a number between 0 and 100
// If too high, display a message on screen : You are too high,
// Low: You are too low,
// Right: YOU ARE RIGHT,
// Show restart button: reset the game.

//Variables

let randomNum = Math.floor(Math.random() * 101);

const guesses = document.querySelector(".guesses");

const lastResult = document.querySelector(".lastResult");

const lowOrHi = document.querySelector(".lowOrHi");

const guessSubmit = document.querySelector(".guessSubmit");

const guessReset = document.querySelector(".guessReset");

const guessField = document.querySelector(".guessField");

let guessCount = 1;


function checkGuess() {
  let userGuess = Number(guessField.value); //Number to parse guess into number value
  if (guessCount === 1) {
    guesses.textContent = "Guesses: ";
  }

  guesses.textContent += userGuess + ",";

  if (userGuess === randomNum) {
    lastResult.textContent = "YOU ARE RIGHT";
    lowOrHi.textContent = "";
    setGameOver();
  } else if (guessCount === 10) {
    lastResult.textContent = "!!!GAME OVER!!!";
    lowOrHi.textContent = "";
    setGameOver();
  } else {
    lastResult.textContent = "Wrong!";

    if (userGuess < randomNum) {
      lowOrHi.textContent = "Your guess is to low!";
    } else if (userGuess > randomNum) {
      lowOrHi.textContent = "Your guess is to high!";
    }

  }
    

  guessCount++;
  guessField.value = "";
  guessField.focus();
}

guessSubmit.addEventListener("click", checkGuess);
guessReset.addEventListener("click", resetGame);

function setGameOver() {
  guessField.disabled = true;
  guessSubmit.disabled = true;
  guessReset.textContent = "Start new game";
  
}

function resetGame() {
  if (!confirm("Are you sure you want to reset?")) {
    return;
  }

  guessCount = 1;
  const resetParas = document.querySelectorAll(".resultParas p");
  for (let i = 0; i < resetParas.length; i++) {
    resetParas[i].textContent = "";
  }

 
  guessField.disabled = false;
  guessSubmit.disabled = false;
  guessField.value = "";
  guessField.focus();
  lastResult.style.backgroundColor = "white";
  randomNum = Math.floor(Math.random() * 101);
}


